# Adaptive Flooding Algorithm

Adaptive flooding algorithm for the computation of workspace of CPRs, planar case. Adaptation in order to certify the solution of the IGS problem

SOME TIPS on how to use the code:

1: create the robot folder (there are 3 robots yet computed), and create a file that containts all the infos about the robot FILE_XXXrobotfile.

2: in FILE_XXXrobotfile, set strain limits, joint limits, and an eventual preconditioner to be used for the Kantorovich certification

3: set the inputfile:
3a: guessflag: choose 'C' to increase computation speed, choose 'K' to increase certification percentage.
3b: solverflag: choose Newton for Kantorovich certification, or choose fsolve for more robust computation
3c: set up parameters: TOL for singularity threshold, TOL2 to avoid jumping T1 singularities with large steps (set as the conditioning of Jacobian matrix near T1 singularies)
3d grid parameters: stepsize is the initial stepsize, set stepsize = stepmin to have fixed grid behaviour. boxsize is the outer limits of the workspace computation

4: run the main file. Activate the desired robot-parameters file, set instanplot = 1 to see plot during computation (it increase considerably the computational time)

This is just a short READMe, not a documentation
