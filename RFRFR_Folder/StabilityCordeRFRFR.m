function flag = StabilityCordeRFRFR(jac)
% jac = full jacobian of the IGSP problem
H = jac(1:end-6,3:end-4); % extract correct terms
Z = null(jac(end-5:end-2,3:end-4)); % compute nullspace of gradients of constraints
Hr = Z'*H*Z; % reduced hessian matrix
[~,flag] = chol(Hr); % attempt cholesky decomposition --> if flag = 0, symmetric positive definite
end