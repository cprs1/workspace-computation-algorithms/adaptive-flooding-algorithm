%% CCInverseRFRFRGuess
% initial guess for non-linear solvers obtained through Constant curvature
% assumption
function output = CCInverseRFRFRGuess(pA,pB,pP,L,N,signA,signB)
% pA: position of motor 1
% pB: position of motor 2
% pos_end: end effector position (w.r.t fixed frame in [0;0])
% N : number of sampling points
% signA: sign of curvature for beam 1
% signB: sign of curvature for beam 2
% only position and orientation are obtained, forces couples are set = 0
output = zeros(12,N);
options = optimoptions('fsolve','Display','off');
%% BEAM A
fun = @(k) (2)./k.*sin(L.*k/2)-norm(pP-pA);
k0 = signA*pi/L;
kA = fsolve(fun,k0,options);
a2A = atan2((pP(2)-pA(2)),(pP(1)-pA(1)));
q = a2A+L*kA/2;
output(3,:) = atan2((pP(2)-pA(2)),(pP(1)-pA(1)))+L*kA/2-linspace(0,(L)*kA,N);
delta = q-pi/2;
C = pA+(1/kA)*[cos(delta);sin(delta)];
th = pi+delta-linspace(0,L*kA,N);
output(1:2,:) = C + (1/kA)*[cos(th);sin(th)];

%% BEAM B
fun = @(k) (2)./k.*sin(L.*k/2)-norm(pP-pB);
k0 = signB*pi/L;
kB = fsolve(fun,k0,options);
a2B = atan2((pP(2)-pB(2)),(pP(1)-pB(1)));
q = a2B+L*kB/2;
output(9,:) = atan2((pP(2)-pB(2)),(pP(1)-pB(1)))+L*kB/2-linspace(0,(L)*kB,N);
delta = q-pi/2;
C = pB+(1/kB)*[cos(delta);sin(delta)];
th = pi+delta-linspace(0,L*kB,N);
output(7:8,:) = C + (1/kB)*[cos(th);sin(th)];

end