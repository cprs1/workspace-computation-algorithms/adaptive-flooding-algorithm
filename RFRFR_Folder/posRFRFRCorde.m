function [pos1,pos2] = posRFRFRCorde(y,Nelt,Lelt,pA1,pA2)
Nth = Nelt-1;
qA1 = y(1,1);
qA2 = y(2,1);
th = y(3:end-4,1);
th1 = th(0*Nth+1:1*Nth,1);
th2 = th(1*Nth+1:2*Nth,1);
alpha1 = [qA1;th1];
alpha2 = [qA2;th2];
pos1 = zeros(2,Nelt+1);
pos2 = zeros(2,Nelt+1);
pos1(:,1) = pA1;
pos2(:,1) = pA2;

% rebuild legs position with recursive formula
for i = 1:Nelt
pos1(:,i+1) = pos1(:,i)+Lelt*[cos(alpha1(i));sin(alpha1(i))];
pos2(:,i+1) = pos2(:,i)+Lelt*[cos(alpha2(i));sin(alpha2(i))];
end

end