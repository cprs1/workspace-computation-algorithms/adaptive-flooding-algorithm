function [flag,chi,delta,lambda] = KantorovichCordeRFRFR(y,eq,jac,Nelt,Le,prec,EI,L,fd)

    % conditioning
    injac = inv(prec*jac);
    eq = prec*eq;
    % conditioning
       chi = norm(injac,Inf);
    % distance from linearized solution
    delta = norm(injac*eq,Inf);
    if delta<(pi/2)
       cdelta = 1;
       sdelta = abs(sin(delta));
    else
        cdelta = 1;
        sdelta = 1;
    end
    md = abs(delta);
    % non linearities
    Nth = Nelt-1;
    qA1 = y(1,1);
    qA2 = y(2,1);
    th = y(3:end-4,1);
    th1 = th(0*Nth+1:1*Nth,1);
    th2 = th(1*Nth+1:2*Nth,1);
    lambda = y(end-3:end,1);
    % constraints hessian
    hc = zeros(4,1);
    hc(1,1) = Le*(abs(cos(qA1))*cdelta + abs(sin(qA1))*sdelta + sum( abs(cos(th1)).*cdelta + abs(sin(th1)).*sdelta ));
    hc(2,1) = Le*(abs(sin(qA1))*cdelta + abs(cos(qA1))*sdelta + sum( abs(sin(th1)).*cdelta + abs(cos(th1)).*sdelta ));
    hc(3,1) = Le*(abs(cos(qA2))*cdelta + abs(sin(qA2))*sdelta + sum( abs(cos(th2)).*cdelta + abs(sin(th2)).*sdelta ));
    hc(4,1) = Le*(abs(sin(qA2))*cdelta + abs(cos(qA2))*sdelta + sum( abs(sin(th2)).*cdelta + abs(cos(th2)).*sdelta ));
    hc = hc/L;
    % energy hessian
    heA1 = 2*Le*(abs(cos(th1)).*cdelta+abs(sin(th1)).*sdelta);
    heA2 = 2*Le*(abs(sin(th1)).*cdelta+abs(cos(th1)).*sdelta);
    heA3 = Le*((abs(lambda(1))+md+abs(fd(1)))*(abs(sin(th1))*cdelta+abs(cos(th1).*sdelta))+(abs(lambda(2))+md+abs(fd(2)))*(abs(cos(th1))*cdelta+abs(sin(th1).*sdelta)));
    heB1 = 2*Le*(abs(cos(th2)).*cdelta+abs(sin(th2)).*sdelta);
    heB2 = 2*Le*(abs(sin(th2)).*cdelta+abs(cos(th2)).*sdelta);
    heB3 = Le*((abs(lambda(3))+md+abs(fd(1)))*(abs(sin(th2))*cdelta+abs(cos(th2).*sdelta))+(abs(lambda(4))+md+abs(fd(2)))*(abs(cos(th2))*cdelta+abs(sin(th2).*sdelta)));
    he = [heA1+heA2+heA3;heB1+heB2+heB3];
    he = he/(EI/L);
    lambda = max([hc;he]);
    % kantorovich constant
    flag = 2*chi*delta*lambda;
end