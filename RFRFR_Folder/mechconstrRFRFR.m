function flag = mechconstrRFRFR(y,th1lim,th2lim,pA1,pA2,L,r,E,stresslim,Nelt)
qA1 = wrapTo2Pi(y(1,1));
qA2 = wrapTo2Pi(y(2,1));
th = y(3:end-4,1);
Pb = th(end-1:end,1);

% mechanical joint constraints
flag1 = (th1lim(1)<=qA1 & qA1<=th1lim(2));      % motor constraint 1
flag2 = (th2lim(1)<=qA2 & qA2<=th2lim(2));  	% motor constraint 2
flag3 = (norm(Pb-pA1)<=L & norm(Pb-pA2)<=L);    % non extensibility (not really necessary)

% maximum stress constraint 
Nth = Nelt-1;
Le = L/Nelt;
th1 = th(0*Nth+1:1*Nth,1);
th2 = th(1*Nth+1:2*Nth,1);
alpha1 = [qA1;th1];
alpha2 = [qA2;th2];
idx = 3:Nelt;
d1 = [(alpha1(idx)-alpha1(idx-1))/Le;0]*r*E;
d2 = [(alpha2(idx)-alpha2(idx-1))/Le;0]*r*E;
s1 = max(((d1).^2.).^0.5);
s2 = max(((d2).^2.).^0.5);
stress = max(s1,s2);

flag4 = stress<=stresslim; % check if strains exceeded
 
flag = (flag1 & flag2 & flag3 & flag4); % true --> everything ok

   
end