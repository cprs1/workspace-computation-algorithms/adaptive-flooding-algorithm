%% IGSP Corde Equations 
% INPUT:
% y = 
% k = equivalent planar stiffness
% Le = element lenght
% fend = end-effector concentrated force
% fd = distributed load (e.g. gravity)
% Pend = end-effector assigned position
% OUTPUT: 
% eq = Inverse geometrico-static model equations as column vector
% gradeq = gradient for non-linear solver

function [eq,gradeq] = CordeIGSPeqnRFRFR(y,k,Le,fend,fd,Pend,pA1,pA2)

gradeq = [];
n = max(size(y));
Nel = (n-6)/2;
Nth = Nel-1;

% Extract Variables
qA1 = y(1,1);
qA2 = y(2,1);
th = y(3:end-4,1);
th1 = th(0*Nth+1:1*Nth,1);
th2 = th(1*Nth+1:2*Nth,1);
Pb = th(end-1:end,1);
lambda = y(end-3:end,1);

% gradL
j = 2:Nth-1;

gV11 = k*(-qA1+2*th1(1)-th1(2))         -.5*Le*fd'*[-sin(th1(1));cos(th1(1))]      +Le*[-sin(th1(1)),cos(th1(1)),0,0]*lambda;
gV1N = k*(th1(end)-th1(end-1))          -.5*Le*fd'*[-sin(th1(end));cos(th1(end))]  +Le*[-sin(th1(end)),cos(th1(end)),0,0]*lambda;
gV1j = k*(-th1(j-1)+2*th1(j)-th1(j+1))  -.5*Le*[-sin(th1(j)),cos(th1(j))]*fd       +Le*[-sin(th1(j)),cos(th1(j)),zeros(numel(j),2)]*lambda;

gV1 = [gV11;gV1j;gV1N];

gV21 = k*(-qA2+2*th2(1)-th2(2))         -.5*Le*fd'*[-sin(th2(1));cos(th2(1))]       +Le*[0,0,-sin(th2(1)),cos(th2(1))]*lambda;
gV2N = k*(th2(end)-th2(end-1))          -.5*Le*fd'*[-sin(th2(end));cos(th2(end))]   +Le*[0,0,-sin(th2(end)),cos(th2(end))]*lambda;
gV2j = k*(-th2(j-1)+2*th2(j)-th2(j+1))  -.5*Le*[-sin(th2(j)),cos(th2(j))]*fd        +Le*[zeros(numel(j),2),-sin(th2(j)),cos(th2(j)),]*lambda;
gV2 = [gV21;gV2j;gV2N];

gVPb = -fend -[eye(2),eye(2)]*lambda;

% phi
phi1 = pA1+[Le*cos(qA1)+sum(Le*cos(th1));Le*sin(qA1)+sum(Le*sin(th1))]-Pb;
phi2 = pA2+[Le*cos(qA2)+sum(Le*cos(th2));Le*sin(qA2)+sum(Le*sin(th2))]-Pb;
phi = [phi1;phi2];
% inv

inv = Pb-Pend; 

% collect eqn
eq = [gV1;gV2;gVPb;phi;inv];

if nargout > 1
    % V1 eq
    hV1q1 = [-k;zeros(Nth-1,1)];
    hV1q2 =  zeros(Nth,1);
    
    hV1t1 = zeros(Nth,Nth);
    hdiag1 = 2*k*diag(ones(Nth,1)) + diag(Le*([+cos(th1),sin(th1)]*.5*fd-[cos(th1),sin(th1),zeros(Nth,2)]*lambda));
    hupper = [zeros(Nth-1,1),-k*diag(ones(Nth-1,1));zeros(1,Nth)];
    hlower = [zeros(1,Nth);-k*diag(ones(Nth-1,1)),zeros(Nth-1,1)];
    hV1t1 = hV1t1 + hdiag1 + hupper + hlower;
    hV1t1(end,end) = k;
 
    hV1t2 = zeros(Nth,Nth);
    hV1Pb = zeros(Nth,2);
    hV1lamb = Le*[-sin(th1),cos(th1),zeros(Nth,2)];
    
    % V2 eq
    hV2q1 =  zeros(Nth,1);
    hV2q2 =  [-k;zeros(Nth-1,1)];
    hV2t1 = zeros(Nth,Nth);

    hV2t2 = zeros(Nth,Nth);
    hdiag2 = 2*k*diag(ones(Nth,1)) + diag(Le*([+cos(th2),sin(th2)]*.5*fd-[zeros(Nth,2),cos(th2),sin(th2)]*lambda));
    hV2t2 = hV2t2 + hdiag2 + hupper + hlower;
    hV2t2(end,end) = k;
    
    hV2Pb = zeros(Nth,2);
    hV2lamb = Le*[zeros(Nth,2),-sin(th2),cos(th2)];
   
    % Pb eq
    hVbq1 = zeros(2,1);
    hVbq2 = zeros(2,1);
    hVbt1 = zeros(2,Nth);
    hVbt2 = zeros(2,Nth);
    hVbPb = zeros(2,2);
    hVblamb = -[eye(2),eye(2)];
    
    % Phi eq
    hPhq1 = Le*[-sin(qA1);cos(qA1);zeros(2,1)];
    hPhq2 = Le*[zeros(2,1);-sin(qA2);cos(qA2)];
    hPht1 = Le*[-sin(th1)';cos(th1)'; zeros(2,Nth)];
    hPht2 = Le*[zeros(2,Nth); -sin(th2)';cos(th2)'];
    hPhPb = -[eye(2);eye(2)];
    hPhlamb = zeros(4,4);
    
    % Inv eq
    hInq1 = zeros(2,1);
    hInq2 = zeros(2,1);
    hInt1 = zeros(2,Nth);
    hInt2 = zeros(2,Nth);
    hInPb = eye(2);
    hInlamb = zeros(2,4);
    
    % assembly hessian
    gradeq = [hV1q1, hV1q2, hV1t1, hV1t2, hV1Pb, hV1lamb;
              hV2q1, hV2q2, hV2t1, hV2t2, hV2Pb, hV2lamb;
              hVbq1, hVbq2, hVbt1, hVbt2, hVbPb, hVblamb;
              hPhq1, hPhq2, hPht1, hPht2, hPhPb, hPhlamb;
              hInq1, hInq2, hInt1, hInt2, hInPb, hInlamb;];
end
end