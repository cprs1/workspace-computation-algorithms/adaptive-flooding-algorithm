%% WORKSPACE COMPUTATION OF PLANAR CONTINUUM PARALLEL ROBOTS
% F. Zaccaria - Update 02 February 2022

% function for saving results:
% INPUTS: 
% y: robot configuration
% jac: robot IGSP jacobian matrix
% flagk: Kantorovich flag (2*chi*delta*lambda)
% fcn: structure with objective functions

% OUTPUTS:
% WK_line: 1x7 array with values to be stocked in WK
function WK_line = saveresults(y,jac,flagk,fcn)
Stability = fcn.stabilityfcn;
Singu = fcn.singufcn;
InternalEnergy = fcn.internfcn;

id_6 = flagk;
id_7 = Stability(jac); 
[id_8,id_9] = Singu(jac);
id_10 = norm(inv(jac),Inf); 
id_11 = InternalEnergy(y);
id_4_5 = [1,1];
                
WK_line = [id_4_5,id_6,id_7,id_8,id_9,id_10,id_11];

end

%: this can be done better: if an additional output is desired, add a new
%column to WK_line and correspondly to WK in the flooding algo