%% WORKSPACE COMPUTATION OF PLANAR CONTINUUM PARALLEL ROBOTS
% F. Zaccaria - Update 02 February 2022

% function to identify guess candidates for the IGSP solution

% INPUTS: 
% WK: workspace data array
% pend: end-effector desired pose
% current-idx: indices of pend
% params: structure with simulation parameters.

% OUTPUTS:
% idm: indices of guess candidates of pend
% idw: indices of neighbors of pend

function [idm,idw] = getGuessCandidates(WK,pend,current_idx,params)
stepsize = params.stepsize;
idw = getNeighbors(WK,pend,params,1.43);
idw = idw((idw~=current_idx)==1);
search_step = WK(current_idx,12);
check=1;
while check==1
    idw_search = ((abs(WK(idw,2)-pend(1))<=1.43*search_step) & (abs(WK(idw,3)-pend(2))<=1.43*search_step));
    idw_search = idw(idw_search==1);
    % find neighbors in WK
    idx_wk =(WK(idw_search,4)==1);
    idx_wk = idw_search(idx_wk==1);
    if numel(idx_wk)~=0
        check=0;
    else
        search_step = 2*search_step;
        if(search_step > stepsize)
            idw = ((abs(WK(:,2)-pend(1))<=1.43*search_step) & (abs(WK(:,3)-pend(2))<=1.43*search_step));
            idw = WK(idw==1,1);
            idw = idw((idw~=current_idx)==1);
        end
    end
end
idm = idx_wk;

end