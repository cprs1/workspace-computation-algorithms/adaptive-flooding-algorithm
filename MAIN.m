%% WORKSPACE COMPUTATION OF PLANAR CONTINUUM PARALLEL ROBOTS
% F. Zaccaria - Update 02 February 2022

% MAIN FILE: run this to do simulations
clear
close all
clc

%% ROBOT PARAMETERS FILE
FILE_RFRFRrobotfile
% FILE_3RFRrobotfile
% FILE_3PFRrobotfile
% FILE_3PFRrobotfile_2


%% SIMULATION PARAMETERS
FILE_inputfile

%%
instantplot = 1;
[WK,outstruct] = FCN_FloodingPlanarWK(fcn,params,instantplot);

outstruct
%%

FCN_plot(WK,params)
