%% WORKSPACE COMPUTATION OF PLANAR CONTINUUM PARALLEL ROBOTS
% F. Zaccaria - Update 02 February 2022

% flooding algorithm function

% INPUTS:
% fcn: structure with objective functions
% params: structure with simulation parameters.
% instantplot: set = 1 to see plot during computation (comp time
% considerably increased)

% OUTPUTS:
% WK: workspace data array
% outstruct: structure with output informations

function [WK,outstruct] = FCN_FloodingPlanarWK(fcn,params,instantplot)
tic;

%% RECOVER INPUTS FROM STRUCTURES
y0 = params.y0;
pstart = params.pstart;
stepsize = params.stepsize;
stepmin = params.stepmin;
TOL2 = params.TOL2;
mech = fcn.mechconstrfcn; 


%% DEFINE GRID

table = defineGrid(params);
np = numel(table(:,1));
idx = 1:1:np;
WK = [idx',table,zeros(np,9)];
WK(:,12) = stepsize;
guesses = zeros(max(size(y0)),np);

%% SOLVE FIRST ITERATION
id2 = getNeighbors(WK,pstart,params,1.43);
pend = [WK(id2(1),2);WK(id2(1),3)];
[y,jac,~] = solveIGSP(fcn,params,pend,y0);
% store results
WK(id2(1),4:11) = saveresults(y,jac,1,fcn);
guesses(:,id2(1)) = y;


% Initialize 
idn2 = getNeighbors(WK,pend,params,1.43);
idn2 = idn2((idn2~=id2(1))==1); % exclude the first point
wk_todo = idn2;
wk_todoend = [];

%% MAIN LOOP
while (numel(wk_todo)>0 || numel(wk_todoend)>0)
    if (numel(wk_todo)==0)
        wk_todo = wk_todoend;
        wk_todoend = [];
    end
    
    %% EXTRACT POINT
    current_idx = wk_todo(1);
    WK(current_idx,5) = 1;
    wk_todo = wk_todo(2:end);
    pend = [WK(current_idx,2);WK(current_idx,3)];
    
    %% CHOOSE GUESS CANDIDATES
    [idmentry,idw] = getGuessCandidates(WK,pend,current_idx,params); 
    
    %% CHOOSE BEST INITIAL GUESS
    [y0,flagk,idm] = getInitialGuess(WK,guesses,idmentry,pend,params,fcn);
    
    %% COMPUTATION
    switch WK(current_idx,12)<= stepmin
        case 1  % you are employing the smaller stepsize, or fixed grid, just compute
            [y,jac,flag] = solveIGSP(fcn,params,pend,y0);
            flags = (flag & norm(inv(jac),Inf)<=TOL2 & mech(y));
            WK(current_idx,4:11) = saveresults(y,jac,flagk,fcn);
            % if ok: not T1 singu and feasible
            if (flags==1)
                guesses(:,current_idx) = y;
                [wk_todo, wk_todoend ,idny2] = toDoManager(WK,params,idw,WK(current_idx,9),wk_todo,wk_todoend);
                WK(idny2,5) = 1;
                if instantplot==1
                    plot(pend(1),pend(2),'b.')
                    hold on
                    drawnow
                end
            % if T1 singu or not feasible
            else
                WK(current_idx,4) = 3; 
                if (flag==1 && (norm(inv(jac),Inf)<=TOL2)==1 && mech(y)==0)
                   WK(current_idx,4) = 2; 
                end
                if instantplot==1
                    plot(pend(1),pend(2),'r.')
                    hold on
                    drawnow
                end
            end
        case 0 % you can bisect, if required
            if flagk<=1 
                [y,jac,flag] = solveIGSP(fcn,params,pend,y0);
                flags = (flag & norm(inv(jac),Inf)<=TOL2 & mech(y));
                WK(current_idx,4:11) = saveresults(y,jac,flagk,fcn);
                % if ok: not T1 singu and feasible
                if (flags==1) 
                    guesses(:,current_idx) = y;
                    [wk_todo, wk_todoend ,idny2] = toDoManager(WK,params,idw,WK(current_idx,9),wk_todo,wk_todoend);
                    WK(idny2,5) = 1;
                    % if T1 singu or not feasible
                else
                    WK(current_idx,4) = 3; 
                    if (flag==1 && (norm(inv(jac),Inf)<=TOL2)==1 && mech(y)==0)
                       WK(current_idx,4) = 2; 
                    end
                end
            % not certitied: bisect
            else 
                pointstart = WK(idm,2:3);
                actual_step = WK(current_idx,12);
                [I,adds] = bisectionProcessManager(actual_step,pointstart,pend) ;
                last_idx = WK(end,1)+[1;2;3];
                WK(current_idx,:) = [current_idx,adds(1,:)];
                WK(last_idx,:) = [last_idx,adds(2:4,:)];
                indices = [current_idx;last_idx];
                indices = indices(I);
                wk_todo = [indices;wk_todo];
            end
        otherwise
            error('FATAL ERROR') % in case strange things happens..
    end
end

%% SAVE DATA
time = toc;
wk_idx = WK(WK(:,4) == 1 & WK(:,7) == 0,1);
unst_idx = WK(WK(:,4) == 1 & WK(:,7) ~= 0,1);
wk_kant = WK(WK(:,4) == 1 & WK(:,7) == 0 & WK(:,6)<1,1);
wk_computed = WK(WK(:,10)~=0,1);
certified = sum(WK(wk_kant,12).^2)/sum(WK(wk_idx,12).^2);
outstruct.time = time/60;
outstruct.computedpoints = numel(wk_computed);
outstruct.stablearea = sum(WK(wk_idx,12).^2);
outstruct.certifiedstablepercentage = certified*100;
outstruct.unstablearea = sum(WK(unst_idx,12).^2);

end