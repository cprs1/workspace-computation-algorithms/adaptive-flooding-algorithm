%% WORKSPACE COMPUTATION OF PLANAR CONTINUUM PARALLEL ROBOTS
% F. Zaccaria - Update 02 February 2022

% function for the solution of the IGSP Problem
% INPUTS:
% fcn: structure with objective functions
% params: structure with simulation parameters.
% pend: end-effector desired pose
% y0: initial guess for the robot configuration
% OUTPUTS:
% y: solution for the robot configuration
% jac: IGSP jacobian matrix at the solution
% flag: solver flag (indicated problem solved or not)

function [y,jac,flag] = solveIGSP(fcn,params,pend,y0)

% extract from structures
Cordefun = fcn.objetivefcn;
feq = @(y) Cordefun(y,pend);
solverflag = params.solver;
maxiter = params.maxiter;

% switch different solver: Newton of built-in fsolve
switch solverflag
    case 'Newton'
        TOL = params.TOL;
        prec = params.prec;
        [y,~,jac,flag,~] = Newton(feq,y0,maxiter,TOL,prec);
    case 'fsolve'
        options = optimoption('fsolve','Display','off','Maxiter',maxiter','Algorithm','trust-region','SpecifyObjectiveGradient',true,'CheckGradients',false);
        [y,~,flag,~,jac] = fsolve(feq,y0,options);
        jac = full(jac); % singularity and stability eval requires full matrices (svd)
end
        
%%
% ----------------------
% Auxiliary function Newton Solver
    function [y,fun,jac,flag,iter] = Newton(eq,y0,maxiter,TOL,prec)
        iter = 0;
        err = 1;
        [fun,jac] = feval(eq,y0);
        while (err>TOL && iter<=maxiter)
            y = y0 - (prec*jac) \ (prec*fun);
            iter = iter + 1;
            [fun,jac] = feval(eq,y);
            err = max(norm(y-y0),norm(fun));
            y0 = y;
        end
        flag = (iter<=maxiter);
    end
end