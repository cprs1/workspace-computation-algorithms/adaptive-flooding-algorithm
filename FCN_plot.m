%% WORKSPACE COMPUTATION OF PLANAR CONTINUUM PARALLEL ROBOTS
% F. Zaccaria - Update 02 February 2022

% function to get plots:
% WK: workspace data array
% params: structure with simulation parameters.

% OUTPUTS:
% 1- plot of workspace with STABLE,UNSTABLE, T1,T2, mechlimits regions
% 2- plot of workspace with certification infos
% 3- plot of workspace with display of internal energy
% 4- plot of workspace with display of singularity conditioning

function FCN_plot(WK,parameters)

TOL = parameters.TOL;
boxsize = parameters.boxsize;

idcomp = (WK(:,10)~=0);
idwk = (WK(:,4) == 1 & WK(:,7) == 0);
idun = (WK(:,4) == 1 & WK(:,7) ~= 0);
idkant = (WK(:,4) == 1 & WK(:,7) == 0 & WK(:,6)<1);
idT1 = (WK(:,4)==3);
idT2 = (WK(:,9)<TOL & WK(:,9)>0);
idlimits = (WK(:,4) == 2);

% PLOT WORKSPACE WITH STABLE,UNSTABLE, T1,T2, mechlimits
figure()
plot(WK(idwk,2),WK(idwk,3),'b.')
hold on
plot(WK(idun,2),WK(idun,3),'y.')
plot(WK(idT2,2),WK(idT2,3),'k.')
plot(WK(idT1,2),WK(idT1,3),'r.')
if numel(idlimits)>0
    plot(WK(idlimits,2),WK(idlimits,3),'.','Color',[0, 0.5, 0])
end
xlabel('x_{[m]}')
ylabel('y_{[m]}')
grid on
axis equal
axis(boxsize)
title('Workspace')

% PLOT WORKSPACE WITH CERTIFICATION INFORMATIONS
figure()
plot(WK(idwk,2),WK(idwk,3),'c.')
hold on
plot(WK(idkant,2),WK(idkant,3),'b.')
plot(WK(idun,2),WK(idun,3),'y.')
plot(WK(idT2,2),WK(idT2,3),'k.')
plot(WK(idT1,2),WK(idT1,3),'r.')
if numel(idlimits)>0
    plot(WK(idlimits,2),WK(idlimits,3),'.','Color',[0, 0.5, 0])
end
xlabel('x_{[m]}')
ylabel('y_{[m]}')
grid on
axis equal
axis(boxsize)
title('Certification')

% PLOT INTERNAL ROBOT ENERGY
en_med = median(WK(idcomp,11));

figure()
scatter(WK(idcomp,2),WK(idcomp,3),[],WK(idcomp,11),'filled')
hold on
plot(WK(idT2,2),WK(idT2,3),'k.')
plot(WK(idT1,2),WK(idT1,3),'r.')
if numel(idlimits)>0
    plot(WK(idlimits,2),WK(idlimits,3),'.','Color',[0, 0.5, 0])
end
xlabel('x_{[m]}')
ylabel('y_{[m]}')
grid on
colorbar
caxis([0 , 2*en_med])
axis equal
axis(boxsize)
title('Interal Energy')


%% PLOT WORKSPACE WITH SINGULARITY CONDITIONING MAPS
figure()
subplot(1,2,1)
t1_med = median(WK(idcomp,8));
scatter(WK(idcomp,2),WK(idcomp,3),[],WK(idcomp,8),'filled')
xlabel('x_{[m]}')
ylabel('y_{[m]}')
grid on
axis equal
axis(boxsize)
colorbar
caxis([0 , 2*t1_med])
title('T1 conditioning')

subplot(1,2,2)
t2_med = median(WK(idcomp,9));
scatter(WK(idcomp,2),WK(idcomp,3),[],WK(idcomp,9),'filled')
xlabel('x_{[m]}')
ylabel('y_{[m]}')
grid on
axis equal
axis(boxsize)
colorbar
caxis([0 , 2*t2_med])
title('T2 conditioning')
end