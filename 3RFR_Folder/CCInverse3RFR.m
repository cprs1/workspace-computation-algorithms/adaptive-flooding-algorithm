%% CCInverseRFRFRGuess
% initial guess for non-linear solvers obtained through Constant curvature
% assumption
function output = CCInverse3RFR(geoparams,pP,th,L,N,signA,signB,signC)
% pA: position of motor 1
% pB: position of motor 2
% pos_end: end effector position (w.r.t fixed frame in [0;0])
% N : number of sampling points
% signA: sign of curvature for beam 1
% signB: sign of curvature for beam 2
% only position and orientation are obtained, forces couples are set = 0
output = zeros(18,N);
options = optimoptions('fsolve','Display','off');
%% PLATFORM POINTS
pA1 = geoparams(:,1);
pA2 = geoparams(:,2);
pA3 = geoparams(:,3);
B1 = geoparams(:,4);
B2 = geoparams(:,5);
B3 = geoparams(:,6);

pB11 = Rz(th)*B1+pP; %wrt fixed frame
pB22 = Rz(th)*B2+pP;
pB33 = Rz(th)*B3+pP;

pB1 = pB33; % assign the correct one
pB2 = pB11;
pB3 = pB22;

%% BEAM A
fun = @(k) (2)./k.*sin(L.*k/2)-norm(pB1-pA1);
k0 = signA*pi/L;
kA = fsolve(fun,k0,options);
a2A = atan2((pB1(2)-pA1(2)),(pB1(1)-pA1(1)));
q = a2A+L*kA/2;
output(3,:) = atan2((pB1(2)-pA1(2)),(pB1(1)-pA1(1)))+L*kA/2-linspace(0,(L)*kA,N);
delta = q-pi/2;
C = pA1+(1/kA)*[cos(delta);sin(delta)];
th = pi+delta-linspace(0,L*kA,N);
output(1:2,:) = C + (1/kA)*[cos(th);sin(th)];

%% BEAM B
fun = @(k) (2)./k.*sin(L.*k/2)-norm(pB2-pA2);
k0 = signB*pi/L;
kB = fsolve(fun,k0,options);
a2B = atan2((pB2(2)-pA2(2)),(pB2(1)-pA2(1)));
q = a2B+L*kB/2;
output(9,:) = atan2((pB2(2)-pA2(2)),(pB2(1)-pA2(1)))+L*kB/2-linspace(0,(L)*kB,N);
delta = q-pi/2;
C = pA2+(1/kB)*[cos(delta);sin(delta)];
th = pi+delta-linspace(0,L*kB,N);
output(7:8,:) = C + (1/kB)*[cos(th);sin(th)];

%% BEAM C
fun = @(k) (2)./k.*sin(L.*k/2)-norm(pB3-pA3);
k0 = signC*pi/L;
kC = fsolve(fun,k0,options);
a2C = atan2((pB3(2)-pA3(2)),(pB3(1)-pA3(1)));
q = a2C+L*kC/2;
output(15,:) = atan2((pB3(2)-pA3(2)),(pB3(1)-pA3(1)))+L*kC/2-linspace(0,(L)*kC,N);
delta = q-pi/2;
C = pA3+(1/kC)*[cos(delta);sin(delta)];
th = pi+delta-linspace(0,L*kC,N);
output(13:14,:) = C + (1/kC)*[cos(th);sin(th)];

end