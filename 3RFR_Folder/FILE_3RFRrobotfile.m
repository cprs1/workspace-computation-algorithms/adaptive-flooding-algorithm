%% ROBOT PARAMETERS 3RFR

%% Robot Geometry Parameters

rB = 0.7;       % base radius [m]
rP = 0.2;       % platform radius [m]
L = 1;          % link lenght [m]

th1 = pi/2; % wrt fixed frame 
th2 = pi/2+2*pi/3;
th3 = pi/2-2*pi/3;

% motor positions at base [m]
A1 = rB*[cos(th1);sin(th1)]; % wrt fixed frame 
A2 = rB*[cos(th2);sin(th2)]; 
A3 = rB*[cos(th3);sin(th3)];

% joint positions at platform
B1 = rP*[cos(th1);sin(th1)]; % wrt platform frame 
B2 = rP*[cos(th2);sin(th2)]; 
B3 = rP*[cos(th3);sin(th3)];

geoparams = [A1,A2,A3,B1,B2,B3]; % collect

th1lim = [-Inf,+Inf]; % motor 1 angular limits [rad] (set -Inf Inf to neglect)
th2lim = [-Inf,+Inf]; % motor 2 angular limits [rad] (set -Inf Inf to neglect)
th3lim = [-Inf,+Inf]; % motor 3 angular limits [rad] (set -Inf Inf to neglect)

%% Robot Material Parameters

r = 0.001; % cross section radius [m]
E = 210*10^9; % young modulus [Pa]
A = pi*r^2; % cross section area [m^2]
I = 0.25*pi*r^4; % cross section inertia moment [m^4]
rho = 7800; % density [kg/m^3]
EI = E*I;

stresslim = Inf; %Maximum puntual stress [Pa] (set Inf to neglect)
 

%% External loads

g = [0;0]; % gravity (set 0 to neglect)
fd = rho*A*g; % distributed loads in global frame [N/m]
fend = [0;0]; % end effector force in global frame [N]
mend = 0; % end effector moment in global frame [Nm]

%% Model Parameters

Nelt = 30; % N. of elements for each link
Le = L/Nelt; % element lenght  
k = EI/Le; % equivalent torsional stress

inflA = +1; % sign of "first attempt" curvature first beam
inflB = +1; % sign of "first attempt" curvature second beam
inflC = +1; % sign of "first attempt" curvature third beam

th = 20*pi/180; % CONSTANT platform orientation wrt fixed frame

pstart = [0;0]; % start position
params.pstart = pstart; % store

%% Preconditioner
prec = eye(99); %set eye to neglect is
params.prec = prec;

%% Simulation functions

%Objective function for IGSP problem
fcn.objetivefcn = @(y,pend) CordeIGSPeqn3RFR(y,k,Le,fend,mend,fd,pend,th,geoparams);

% Kantorovich-Constant function
fcn.kantorovichfcn = @(y,eq,jac) KantorovichCorde3RFR(y,eq,jac,Nelt,Le,geoparams,prec,EI,L,fd);

% Mechanical constraint evaluation function
fcn.mechconstrfcn = @(y) mechconstr3RFR(y,th1lim,th2lim,th3lim,Nelt,L,r,E,stresslim);

% Singularity evaluation function
fcn.singufcn = @(jac) SinguCorde3RFR(jac);

% Stability evaluation function
fcn.stabilityfcn = @(jac) StabilityCorde3RFR(jac);

% Robot internal energy function
fcn.internfcn = @(y) internalEnergy3RFR(y,k);

%% Solution of the first IGSP

output = CCInverse3RFR(geoparams,pstart,th,L,Nelt,inflA,inflB,inflC); % constant curvature approximation
alpha1 = output(03,:)';
alpha2 = output(09,:)';
alpha3 = output(15,:)';
y0 = [alpha1(1);alpha2(1);alpha3(1);alpha1(2:end);alpha2(2:end);alpha3(2:end);pstart;th;zeros(6,1)]; % constant curvature initial guess
options = optimoptions('fsolve','Display','iter-detailed');
fun = @(y) CordeIGSPeqn3RFR(y,k,Le,fend,mend,fd,pstart,th,geoparams);
sol = fsolve(fun,y0,options);

params.y0 = sol; % store 
[pos1,pos2,pos3] = pos3RFR(sol,Nelt,Le,geoparams); % function for links reconstruction
Plot3RFR(pos1,pos2,pos3,pstart,th,geoparams,L) % robot plot function
title('Initial Guess Configuration')
drawnow % to draw immediately
