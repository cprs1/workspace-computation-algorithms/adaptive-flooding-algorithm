function [eq,gradeq] = CordeIGSPeqn3RFR(y,k,Le,fend,mend,fd,pend,thP,geoparams)
% y = generalized coordinates = [qa1,qa2,th1,th2,Pb,lambda]
% k = equivalent planar stiffness
% Le = element lenght
% fend = end-effector concentrated force
% fd = distributed load (e.g. gravity)
% pend = end-effector assigned position
% thP = end-effector orientation
% OUTPUT: 
% eq = Inverse geometrico-static model equations as column vector
% gradeq = gradient for non-linear solver
gradeq = [];

n = max(size(y));
Nel = (n-9)/3;
Nth = Nel-1;
% geometric params
pA1 = geoparams(:,1);
pA2 = geoparams(:,2);
pA3 = geoparams(:,3);
pB1 = geoparams(:,4);
pB2 = geoparams(:,5);
pB3 = geoparams(:,6);

% Extract Variables
qA1 = y(1,1);
qA2 = y(2,1);
qA3 = y(3,1);
th = y(4:end-6,1);
th1 = th(0*Nth+1:1*Nth,1);
th2 = th(1*Nth+1:2*Nth,1);
th3 = th(2*Nth+1:3*Nth,1);
Pb = th(end-2:end-1,1);
thb = th(end,1);
lambda = y(end-5:end,1);

 % TO DO
% gradL
j = 2:Nth-1;
% 
gV11 = k*(-qA1+2*th1(1)-th1(2))         -.5*Le*fd'*[-sin(th1(1));cos(th1(1))]      +Le*[-sin(th1(1)),cos(th1(1)),0,0,0,0]*lambda;
gV1N = k*(th1(end)-th1(end-1))          -.5*Le*fd'*[-sin(th1(end));cos(th1(end))]  +Le*[-sin(th1(end)),cos(th1(end)),0,0,0,0]*lambda;
gV1j = k*(-th1(j-1)+2*th1(j)-th1(j+1))  -.5*Le*[-sin(th1(j)),cos(th1(j))]*fd       +Le*[-sin(th1(j)),cos(th1(j)),zeros(numel(j),4)]*lambda;

gV1 = [gV11;gV1j;gV1N];
% 
gV21 = k*(-qA2+2*th2(1)-th2(2))         -.5*Le*fd'*[-sin(th2(1));cos(th2(1))]       +Le*[0,0,-sin(th2(1)),cos(th2(1)),0,0]*lambda;
gV2N = k*(th2(end)-th2(end-1))          -.5*Le*fd'*[-sin(th2(end));cos(th2(end))]   +Le*[0,0,-sin(th2(end)),cos(th2(end)),0,0]*lambda;
gV2j = k*(-th2(j-1)+2*th2(j)-th2(j+1))  -.5*Le*[-sin(th2(j)),cos(th2(j))]*fd        +Le*[zeros(numel(j),2),-sin(th2(j)),cos(th2(j)),zeros(numel(j),2)]*lambda;
gV2 = [gV21;gV2j;gV2N];
%
gV31 = k*(-qA3+2*th3(1)-th3(2))         -.5*Le*fd'*[-sin(th3(1));cos(th3(1))]       +Le*[0,0,0,0,-sin(th3(1)),cos(th3(1))]*lambda;
gV3N = k*(th3(end)-th3(end-1))          -.5*Le*fd'*[-sin(th3(end));cos(th3(end))]   +Le*[0,0,0,0,-sin(th3(end)),cos(th3(end))]*lambda;
gV3j = k*(-th3(j-1)+2*th3(j)-th3(j+1))  -.5*Le*[-sin(th3(j)),cos(th3(j))]*fd        +Le*[zeros(numel(j),4),-sin(th3(j)),cos(th3(j))]*lambda;
gV3 = [gV31;gV3j;gV3N];
%
gVpP = -fend -[eye(2),eye(2),eye(2)]*lambda;
%
gVthb = -mend - [Rz(thb+pi/2)*pB3;Rz(thb+pi/2)*pB1;Rz(thb+pi/2)*pB2]'*lambda; 
% phi
phi1 = pA1+[Le*cos(qA1)+sum(Le*cos(th1));Le*sin(qA1)+sum(Le*sin(th1))]-(Rz(thb)*pB3+Pb); %check for correct assembly!
phi2 = pA2+[Le*cos(qA2)+sum(Le*cos(th2));Le*sin(qA2)+sum(Le*sin(th2))]-(Rz(thb)*pB1+Pb);
phi3 = pA3+[Le*cos(qA3)+sum(Le*cos(th3));Le*sin(qA3)+sum(Le*sin(th3))]-(Rz(thb)*pB2+Pb);
phi = [phi1;phi2;phi3];
% % inv
% 
inv = [Pb-pend;thb-thP];
% 
% collect eqn
eq = [gV1;gV2;gV3;gVpP;gVthb;phi;inv];

if nargout > 1
       % V1 eq
    hV1q1 = [-k;zeros(Nth-1,1)];
    hV1q2 = zeros(Nth,1);
    hV1q3 = zeros(Nth,1);
    
    hV1t1 = zeros(Nth,Nth);
    hdiag1 = 2*k*diag(ones(Nth,1)) + diag(Le*([+cos(th1),sin(th1)]*fd*.5-[cos(th1),sin(th1),zeros(Nth,4)]*lambda));
    hupper = [zeros(Nth-1,1),-k*diag(ones(Nth-1,1));zeros(1,Nth)];
    hlower = [zeros(1,Nth);-k*diag(ones(Nth-1,1)),zeros(Nth-1,1)];
    hV1t1 = hV1t1 + hdiag1 + hupper + hlower;
    hV1t1(end,end) = k;
 
    hV1t2 = zeros(Nth,Nth);
    hV1t3 = zeros(Nth,Nth);
    hV1Pb = zeros(Nth,2);
    hV1th = zeros(Nth,1);
    hV1lamb = Le*[-sin(th1),cos(th1),zeros(Nth,4)];
    
    % V2 eq
    hV2q1 = zeros(Nth,1);
    hV2q2 = [-k;zeros(Nth-1,1)];
    hV2q3 = zeros(Nth,1);
    
    hV2t1 = zeros(Nth,Nth); 

    hV2t2 = zeros(Nth,Nth);
    hdiag2 = 2*k*diag(ones(Nth,1)) + diag(Le*([+cos(th2),sin(th2)]*fd*.5-[zeros(Nth,2),cos(th2),sin(th2),zeros(Nth,2)]*lambda));
    hV2t2 = hV2t2 + hdiag2 + hupper + hlower;
    hV2t2(end,end) = k;

    hV2t3 = zeros(Nth,Nth);

    hV2Pb = zeros(Nth,2);
    hV2th = zeros(Nth,1);
    
    hV2lamb = Le*[zeros(Nth,2),-sin(th2),cos(th2),zeros(Nth,2)];
   
    % V3 eq
    hV3q1 = zeros(Nth,1);
    hV3q2 = zeros(Nth,1);
    hV3q3 = [-k;zeros(Nth-1,1)];
        
    hV3t1 = zeros(Nth,Nth); 

    hV3t3 = zeros(Nth,Nth);
    hdiag3 = 2*k*diag(ones(Nth,1)) + diag(Le*([+cos(th3),sin(th3)]*fd*.5-[zeros(Nth,4),cos(th3),sin(th3)]*lambda));
    hV3t3 = hV3t3 + hdiag3 + hupper + hlower;
    hV3t3(end,end) = k;

    hV3t2 = zeros(Nth,Nth);

    hV3Pb = zeros(Nth,2);
    hV3th = zeros(Nth,1);
    
    hV3lamb = Le*[zeros(Nth,4),-sin(th3),cos(th3)];
    
    % Pb eq
    hVbq1 = zeros(2,1);
    hVbq2 = zeros(2,1);
    hVbq3 = zeros(2,1);
    hVbt1 = zeros(2,Nth);
    hVbt2 = zeros(2,Nth);
    hVbt3 = zeros(2,Nth);
    hVbPb = zeros(2,2);
    hVbth = zeros(2,1);
    hVblamb = -[eye(2),eye(2),eye(2)];
    
    % th eq
    hVtq1 = zeros(1,1); %to do
    hVtq2 = zeros(1,1);
    hVtq3 = zeros(1,1);
    hVtt1 = zeros(1,Nth);
    hVtt2 = zeros(1,Nth);
    hVtt3 = zeros(1,Nth);
    hVtPb = zeros(1,2);
    hVtth = -[Rz(thb+pi)*pB3;Rz(thb+pi)*pB1;Rz(thb+pi)*pB2]'*lambda;
    hVtlamb = -[Rz(thb+pi/2)*pB3;Rz(thb+pi/2)*pB1;Rz(thb+pi/2)*pB2]';
    
    % Phi eq
    hPhq1 = Le*[-sin(qA1);cos(qA1);zeros(2,1);zeros(2,1)];
    hPhq2 = Le*[zeros(2,1);-sin(qA2);cos(qA2);zeros(2,1)];
    hPhq3 = Le*[zeros(2,1);zeros(2,1);-sin(qA3);cos(qA3)];
    
    hPht1 = Le*[-sin(th1)';cos(th1)'; zeros(4,Nth)];
    hPht2 = Le*[zeros(2,Nth); -sin(th2)';cos(th2)';zeros(2,Nth)];
    hPht3 = Le*[zeros(4,Nth);-sin(th3)';cos(th3)'];
    
    hPhPb = -[eye(2);eye(2);eye(2)];
    hPhth = -[Rz(thb+pi/2)*pB3;Rz(thb+pi/2)*pB1;Rz(thb+pi/2)*pB2]; %
    hPhlamb = zeros(6,6);
    
    % Inv eq
    hInq1 = zeros(3,1);
    hInq2 = zeros(3,1);
    hInq3 = zeros(3,1);
    hInt1 = zeros(3,Nth);
    hInt2 = zeros(3,Nth);
    hInt3 = zeros(3,Nth);
    hInPb = [eye(2);zeros(1,2)];
    hInth = [0;0;1];
    hInlamb = zeros(3,6);
    
    % assembly hessian
    gradeq = [hV1q1, hV1q2, hV1q3, hV1t1, hV1t2, hV1t3, hV1Pb, hV1th, hV1lamb;
              hV2q1, hV2q2, hV2q3, hV2t1, hV2t2, hV2t3, hV2Pb, hV2th, hV2lamb;
              hV3q1, hV3q2, hV3q3, hV3t1, hV3t2, hV3t3, hV3Pb, hV3th, hV3lamb;
              hVbq1, hVbq2, hVbq3, hVbt1, hVbt2, hVbt3, hVbPb, hVbth, hVblamb;
              hVtq1, hVtq2, hVtq3, hVtt1, hVtt2, hVtt3, hVtPb, hVtth, hVtlamb;
              hPhq1, hPhq2, hPhq3, hPht1, hPht2, hPht3, hPhPb, hPhth, hPhlamb;
              hInq1, hInq2, hInq3, hInt1, hInt2, hInt3, hInPb, hInth, hInlamb;];
end