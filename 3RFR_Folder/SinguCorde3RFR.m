function [flag1,flag2] = SinguCorde3RFR(jac)
% jac is the full jacobian matrix of the IGSP

Z = null(jac(end-8:end-3,4:end-6)); % null space of gradient of constraints

% first group
Jth1 = jac(1:end-9,4:end-9);
Ja1 =  jac(1:end-9,1:3);
Jp1 =  jac(1:end-9,end-8:end-6);
% second group
Jth2 = jac(end-8:end-3,4:end-9);
Ja2 =  jac(end-8:end-3,1:3);
Jp2 =  jac(end-8:end-3,end-8:end-6);
% collect everything
Jth = [Z'*Jth1;Jth2];
Jp = [Z'*Jp1;Jp2];
Ja = [Z'*Ja1;Ja2];

% type 1 and type 2 matrices
typeI  = [Jth,Ja];
typeII = [Jth,Jp];
% estimate inverse conditioning --> if flag = 0, is singular
flag1 = rcond(typeI);
flag2 = rcond(typeII);
end