function [pos1,pos2,pos3] = pos3RFR(y,Nelt,Lelt,geoparams)
% recover position for the 3 RFR
Nth = Nelt-1;
qA1 = y(1,1);
qA2 = y(2,1);
qA3 = y(3,1);
th = y(4:end-6,1);
th1 = th(0*Nth+1:1*Nth,1);
th2 = th(1*Nth+1:2*Nth,1);
th3 = th(2*Nth+1:3*Nth,1);
alpha1 = [qA1;th1];
alpha2 = [qA2;th2];
alpha3 = [qA3;th3];

pos1 = zeros(2,Nelt+1);
pos2 = zeros(2,Nelt+1);
pos3 = zeros(2,Nelt+1);
pos1(:,1) = geoparams(:,1);
pos2(:,1) = geoparams(:,2);
pos3(:,1) = geoparams(:,3);

% recover position thanks to recursive formula
for i = 1:Nelt
pos1(:,i+1) = pos1(:,i)+Lelt*[cos(alpha1(i));sin(alpha1(i))];
pos2(:,i+1) = pos2(:,i)+Lelt*[cos(alpha2(i));sin(alpha2(i))];
pos3(:,i+1) = pos3(:,i)+Lelt*[cos(alpha3(i));sin(alpha3(i))];
end

end