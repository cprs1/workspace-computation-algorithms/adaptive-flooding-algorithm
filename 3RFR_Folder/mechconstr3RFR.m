function flag = mechconstr3RFR(y,th1lim,th2lim,th3lim,Nelt,L,r,E,stresslim)
% evaluate mechanical constrains of the 3RFR robot

qA1 = wrapTo2Pi(y(1,1));
qA2 = wrapTo2Pi(y(2,1));
qA3 = wrapTo2Pi(y(3,1));

% joint limit constraints
flag1 = (th1lim(1)<=qA1 & qA1<=th1lim(2));
flag2 = (th2lim(1)<=qA2 & qA2<=th2lim(2));
flag3 = (th3lim(1)<=qA3 & qA3<=th3lim(2));


% max stress on beams
Nth = Nelt-1;
Le = L/Nelt;

th = y(4:end-6,1);
th1 = th(0*Nth+1:1*Nth,1);
th2 = th(1*Nth+1:2*Nth,1);
th3 = th(2*Nth+1:3*Nth,1);
alpha1 = [qA1;th1];
alpha2 = [qA2;th2];
alpha3 = [qA3;th3];
idx = 3:Nelt;
d1 = [(alpha1(idx)-alpha1(idx-1))/Le;0]*r*E;
d2 = [(alpha2(idx)-alpha2(idx-1))/Le;0]*r*E;
d3 = [(alpha3(idx)-alpha3(idx-1))/Le;0]*r*E;
s1 = max(((d1).^2.).^0.5);
s2 = max(((d2).^2.).^0.5);
s3 = max(((d3).^2.).^0.5);
stress = max([s1,s2,s3]); % evaluate max stress
flag4 = stress<=stresslim; % check if valid

flag = (flag1 & flag2 & flag3 & flag4); % if = true --> everything fine
end