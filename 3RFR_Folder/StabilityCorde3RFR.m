function flag = StabilityCorde3RFR(jac)
% jac is the full jacobian of the IGSP

H = jac(1:end-9,4:end-6); % extract good components
Z = null(jac(end-8:end-3,4:end-6)); % compute nullspace of gradient of constrains
Hr = Z'*H*Z; % reduced hessian matrix
[~,flag] = chol(Hr); % attempt to do cholesky decomposition --> if flag = 0, positive definite
end