function flag = mechconstr3PFR(y,actlim,r,E,L,Nelt,qm1,qm2,qm3,stresslim)
% mechanical constraints for 3PFR

lA1 = y(1,1);
lA2 = y(2,1);
lA3 = y(3,1);

% mechanical rails limits
flag1 = (actlim(1)<=lA1 & lA1<=actlim(2));
flag2 = (actlim(1)<=lA2 & lA2<=actlim(2));
flag3 = (actlim(1)<=lA3 & lA3<=actlim(2));

% maximum stresses on beams
Nth = Nelt-1;
Le = L/Nelt;

th = y(4:end-6,1);
th1 = th(0*Nth+1:1*Nth,1);
th2 = th(1*Nth+1:2*Nth,1);
th3 = th(2*Nth+1:3*Nth,1);
alpha1 = [qm1-pi/2;th1];
alpha2 = [qm2-pi/2;th2];
alpha3 = [qm3-pi/2;th3];
idx = 3:Nelt;
d1 = [(alpha1(idx)-alpha1(idx-1))/Le;0]*r*E;
d2 = [(alpha2(idx)-alpha2(idx-1))/Le;0]*r*E;
d3 = [(alpha3(idx)-alpha3(idx-1))/Le;0]*r*E;
s1 = max(((d1).^2.).^0.5);
s2 = max(((d2).^2.).^0.5);
s3 = max(((d3).^2.).^0.5);
stress = max([s1,s2,s3]); % evaluate max stress
flag4 = stress<=stresslim; % check if it is allowed

flag = (flag1 & flag2 & flag3 & flag4); % if = true, everything ok
end