function [pos1,pos2,pos3] = pos3PRFCorde(y,Nelt,Lelt,geoparams,qm1,qm2,qm3)
% compute position of the 3PFR
Nth = Nelt-1;
th = y(4:end-6,1);
lA1 = y(1,1);
lA2 = y(2,1);
lA3 = y(3,1);

th1 = th(0*Nth+1:1*Nth,1);
th2 = th(1*Nth+1:2*Nth,1);
th3 = th(2*Nth+1:3*Nth,1);
alpha1 = [qm1-pi/2;th1];
alpha2 = [qm2-pi/2;th2];
alpha3 = [qm3-pi/2;th3];

pos1 = zeros(2,Nelt+1);
pos2 = zeros(2,Nelt+1);
pos3 = zeros(2,Nelt+1);
pos1(:,1) = geoparams(:,1) + Rz(qm1)*([0;lA1]);
pos2(:,1) = geoparams(:,2) + Rz(qm2)*([0;lA2]);
pos3(:,1) = geoparams(:,3) + Rz(qm3)*([0;lA3]);

% recursive formula for position recovery
for i = 1:Nelt
pos1(:,i+1) = pos1(:,i)+Lelt*[cos(alpha1(i));sin(alpha1(i))];
pos2(:,i+1) = pos2(:,i)+Lelt*[cos(alpha2(i));sin(alpha2(i))];
pos3(:,i+1) = pos3(:,i)+Lelt*[cos(alpha3(i));sin(alpha3(i))];
end

end