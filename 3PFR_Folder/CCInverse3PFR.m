function [output,lA] = CCInverse3PFR(geoparams,pP,th,N,th1,th2,th3,L)

% CI SONO DEGLI ERRORI!!!!

output = zeros(18,N);
pA1 = geoparams(:,1);
pA2 = geoparams(:,2);
pA3 = geoparams(:,3);
B1 = geoparams(:,4);
B2 = geoparams(:,5);
B3 = geoparams(:,6);

pB11 = Rz(th)*B1+pP; %wrt fixed frame
pB22 = Rz(th)*B2+pP;
pB33 = Rz(th)*B3+pP;

pB1 = pB33; % assign the correct one
pB2 = pB11;
pB3 = pB22;

%% BEAM A
fun = @(unk) solver(unk,pA1,th1-pi/2,pB1,L);
y0 = [100;.1];
sol = fsolve(fun,y0);
xx = sol(1);
lA(1,1) = xx;
kk1 = sol(2);
posA = pA1+Rz(th1)*[0;xx];
d1 = th1-pi/2-pi;
posB = posA+(1/kk1)*[cos(d1);sin(d1)];
t1 = pi+d1-linspace(0,L*kk1,N);
posbb = posB + (1/kk1)*[cos(t1);sin(t1)];

output(1:2,:) = posbb;
output(3,:) = t1;

%% BEAM B
fun = @(unk) solver(unk,pA2,th2-pi/2,pB2,L);
y0 = [100;.1];
sol = fsolve(fun,y0);
xx = sol(1);
lA(2,1) = xx;
kk1 = sol(2);
posA = pA2+Rz(th2)*[0;xx];
d1 = th2-pi/2-pi;
posB = posA+(1/kk1)*[cos(d1);sin(d1)];
t1 = pi+d1-linspace(0,L*kk1,N);
posbb = posB + (1/kk1)*[cos(t1);sin(t1)];

output(7:8,:) = posbb;
output(9,:) = t1;

%% BEAM C
fun = @(unk) solver(unk,pA3,th3-pi/2,pB3,L);
y0 = [100;.1];
sol = fsolve(fun,y0);
xx = sol(1);
lA(3,1) = xx;
kk1 = sol(2);
posA = pA3+Rz(th3)*[0;xx];
d1 = th3-pi/2-pi;
posB = posA+(1/kk1)*[cos(d1);sin(d1)];
t1 = pi+d1-linspace(0,L*kk1,N);
posbb = posB + (1/kk1)*[cos(t1);sin(t1)];

output(13:14,:) = posbb;
output(15,:) = t1;

%%
    function res = solver(unk,pA,qm,Pcd,L)
        
        x = unk(1);
        k1 = unk(2);
        Pa1 = pA+Rz(qm)*[0;x];
        delta1 = qm-pi;
        Pb1 = Pa1+(1/k1)*[cos(delta1);sin(delta1)];
        theta1 = pi+delta1-L*k1;
        res = Pb1 + (1/k1)*[cos(theta1);sin(theta1)]-Pcd; 
    end
end