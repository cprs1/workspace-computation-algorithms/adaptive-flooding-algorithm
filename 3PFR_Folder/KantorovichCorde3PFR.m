function [flag,chi,delta,lambda] = KantorovichCorde3PFR(y,eq,jac,Nelt,Le,geoparams,prec,L,EI)
% kntorovich flags for 3PFR robot
    ppA = geoparams(:,4);
    ppB = geoparams(:,5);
    ppC = geoparams(:,6);
    ppA1 = abs(ppA(1));
    ppA2 = abs(ppA(2));
    ppB1 = abs(ppB(1));
    ppB2 = abs(ppB(2));
    ppC1 = abs(ppC(1));
    ppC2 = abs(ppC(2));
    
    injac = inv(prec*jac);
    eq = prec*eq;
    % conditioning
    chi = norm(injac,Inf);
    % distance from linearized solution
    delta = norm(injac*eq,Inf);
    if delta<(pi/2)
       cdelta = 1;
       sdelta = abs(sin(delta));
    else
        cdelta = 1;
        sdelta = 1;
    end
    md = abs(delta);
    % non linearities
    Nth = Nelt-1;
    qA1 = y(1,1);
    qA2 = y(2,1);
    qA3 = y(3,1);
    th = y(4:end-6,1);
    th1 = th(0*Nth+1:1*Nth,1);
    th2 = th(1*Nth+1:2*Nth,1);
    th3 = th(2*Nth+1:3*Nth,1);
    thp = th(end,1);
    lambda = y(end-5:end,1);
    % constraints hessian
    hc = zeros(6,1);
    hc(1,1) = Le*(sum( abs(cos(th1)).*cdelta + abs(sin(th1)).*sdelta ))+abs(sin(thp))*(ppA2*cdelta+ppA1*sdelta)+abs(cos(thp))*(ppA1*cdelta+ppA2*sdelta);
    hc(2,1) = Le*(sum( abs(sin(th1)).*cdelta + abs(cos(th1)).*sdelta ))+abs(sin(thp))*(ppA1*cdelta+ppA2*sdelta)+abs(cos(thp))*(ppA2*cdelta+ppA1*sdelta);
    hc(3,1) = Le*(sum( abs(cos(th2)).*cdelta + abs(sin(th2)).*sdelta ))+abs(sin(thp))*(ppB2*cdelta+ppB1*sdelta)+abs(cos(thp))*(ppB1*cdelta+ppB2*sdelta);
    hc(4,1) = Le*(sum( abs(sin(th2)).*cdelta + abs(cos(th2)).*sdelta ))+abs(sin(thp))*(ppB1*cdelta+ppB2*sdelta)+abs(cos(thp))*(ppB2*cdelta+ppB1*sdelta);
    hc(5,1) = Le*(sum( abs(cos(th3)).*cdelta + abs(sin(th3)).*sdelta ))+abs(sin(thp))*(ppC2*cdelta+ppC1*sdelta)+abs(cos(thp))*(ppC1*cdelta+ppC2*sdelta);
    hc(6,1) = Le*(sum( abs(sin(th3)).*cdelta + abs(cos(th3)).*sdelta ))+abs(sin(thp))*(ppC1*cdelta+ppC2*sdelta)+abs(cos(thp))*(ppC2*cdelta+ppC1*sdelta);
    hc = hc/L;
    % energy hessian
    heA1 = 2*Le*(abs(cos(th1)).*cdelta+abs(sin(th1)).*sdelta);
    heA2 = 2*Le*(abs(sin(th1)).*cdelta+abs(cos(th1)).*sdelta);
    heA3 = Le*((abs(lambda(1))+md)*(abs(sin(th1))*cdelta+abs(cos(th1).*sdelta))+(abs(lambda(2))+md)*(abs(cos(th1))*cdelta+abs(sin(th1).*sdelta)));
    heB1 = 2*Le*(abs(cos(th2)).*cdelta+abs(sin(th2)).*sdelta);
    heB2 = 2*Le*(abs(sin(th2)).*cdelta+abs(cos(th2)).*sdelta);
    heB3 = Le*((abs(lambda(3))+md)*(abs(sin(th2))*cdelta+abs(cos(th2).*sdelta))+(abs(lambda(4))+md)*(abs(cos(th2))*cdelta+abs(sin(th2).*sdelta)));
    heC1 = 2*Le*(abs(cos(th3)).*cdelta+abs(sin(th3)).*sdelta);
    heC2 = 2*Le*(abs(sin(th3)).*cdelta+abs(cos(th3)).*sdelta);
    heC3 = Le*((abs(lambda(5))+md)*(abs(sin(th2))*cdelta+abs(cos(th2).*sdelta))+(abs(lambda(6))+md)*(abs(cos(th2))*cdelta+abs(sin(th2).*sdelta)));
    he = [heA1+heA2+heA3;heB1+heB2+heB3;heC1+heC2+heC3];
    he = he * (L/EI);
    % platform eqn
    hp1 = 2*(ppA1+ppA2)*(abs(cos(thp))+abs(sin(thp)))*(cdelta+sdelta);
    hp2 = 2*(ppB1+ppB2)*(abs(cos(thp))+abs(sin(thp)))*(cdelta+sdelta);
    hp3 = 2*(ppC1+ppC2)*(abs(cos(thp))+abs(sin(thp)))*(cdelta+sdelta);
    hm11 = (abs(sin(thp))*cdelta+abs(cos(thp))*sdelta)*((abs(lambda(2))+md)*ppA2+(abs(lambda(1))+md)*ppA1);
    hm12 = (abs(cos(thp))*cdelta+abs(sin(thp))*sdelta)*((abs(lambda(1))+md)*ppA2+(abs(lambda(2))+md)*ppA1);
    hm1 = hm11+hm12;
    hm21 = (abs(sin(thp))*cdelta+abs(cos(thp))*sdelta)*((abs(lambda(4))+md)*ppA2+(abs(lambda(3))+md)*ppA1);
    hm22 = (abs(cos(thp))*cdelta+abs(sin(thp))*sdelta)*((abs(lambda(3))+md)*ppA2+(abs(lambda(4))+md)*ppA1);
    hm2 = hm21+hm22;
    hm31 = (abs(sin(thp))*cdelta+abs(cos(thp))*sdelta)*((abs(lambda(6))+md)*ppA2+(abs(lambda(5))+md)*ppA1);
    hm32 = (abs(cos(thp))*cdelta+abs(sin(thp))*sdelta)*((abs(lambda(5))+md)*ppA2+(abs(lambda(6))+md)*ppA1);
    hm3 = hm31+hm32;
    hm = hp1+hp2+hp3+hm1+hm2+hm3;
    hm = hm/L;
    lambda = max([hc;he;hm]);
    % kantorovich constant
    flag = 2*chi*delta*lambda;
end