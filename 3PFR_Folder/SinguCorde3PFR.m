function [flag1,flag2] = SinguCorde3PFR(hess)

Z = null(hess(end-8:end-3,4:end-6));
Jth1 = hess(1:end-9,4:end-9);
Ja1 =  hess(1:end-9,1:3);
Jp1 =  hess(1:end-9,end-8:end-6);

Jth2 = hess(end-8:end-3,4:end-9);
Ja2 =  hess(end-8:end-3,1:3);
Jp2 =  hess(end-8:end-3,end-8:end-6);

Jth = [Z'*Jth1;Jth2];
Jp = [Z'*Jp1;Jp2];
Ja = [Z'*Ja1;Ja2];

typeI  = [Jth,Ja];
typeII = [Jth,Jp];

flag1 = rcond(typeI);
flag2 = rcond(typeII);
end