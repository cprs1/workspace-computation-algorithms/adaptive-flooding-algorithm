%% 3PFR ROBOT

rB = 0.10;   % base radius [m]
rP = 0.15;   % platform radius [m]

th1 = 0; % wrt fixed frame 
th2 = +2*pi/3;
th3 = -2*pi/3;

A1 = rB*[sin(th1);cos(th1)]; % wrt fixed frame 
A2 = rB*[-sin(th2);cos(th2)]; 
A3 = rB*[-sin(th3);cos(th3)];

B1 = rP*[cos(th1);sin(th1)]; % wrt platform frame 
B2 = rP*[cos(th2);sin(th2)]; 
B3 = rP*[cos(th3);sin(th3)];

% basepoints = [A1,A2,A3];
% platformpoints = [B1,B2,B3];

baseangles = [th1,th2,th3];
geoparams = [A1,A2,A3,B3,B1,B2]; % collect

%% External loads

g = [0;0];
fd = rho*A*g;
fend = [0;0];
mend = 0;

wp = [mend;fend];
wd = [0;fd]; % distributed wrench

%% Robot Material Parameters

r = 0.001; % cross section radius [m]
E = 210*10^9; % young modulus [Pa]
A = pi*r^2; % cross section area [m^2]
I = 0.25*pi*r^4; % cross section inertia moment [m^4]
rho = 7800; % density [kg/m^3]
EI = E*I;

stresslim = Inf; %Maximum puntual stress [Pa] (set Inf to neglect)
lA1lim = [0;2]; % actuator rails limits (all equals, set -Inf,+Inf to neglect) [m]

%% External loads

g = [0;0]; % gravity (set 0 to neglect)
fd = rho*A*g; % distributed loads in global frame [N/m]
fend = [0;0]; % end effector force in global frame [N]
mend = 0; % end effector moment in global frame [Nm]

%% Model Parameters

Nelt = 30; % N. of elements for each link
Le = L/Nelt; % element lenght  
k = EI/Le; % equivalent torsional stress

th = -30*pi/180; % CONSTANT platform orientation wrt fixed frame

pstart = [0.5;0]; % start position
params.pstart = pstart; % store

%% Preconditioner
first = ones(3*Nelt-3,1)*(L/EI);
second = [1/L;1/L;1];
last = ones(9,1)*(1/L);
dd = [first;second;last];
prec = diag(dd); % set preconditioner to identity matrix to NOT use it
params.prec = prec;

%% Simulation functions

%Objective function for IGSP problem
fcn.objetivefcn = @(y,pend) CordeIGSPeqn3PFR(y,k,Le,fend,mend,fd,pend,th,geoparams,th1,th2,th3);

% Kantorovich-Constant function
fcn.kantorovichfcn = @(y,eq,jac) KantorovichCorde3PFR(y,eq,jac,Nelt,Le,geoparams,prec,L,EI);

% Mechanical constraint evaluation function
fcn.mechconstrfcn = @(y) mechconstr3PFR(y,lA1lim,r,E,L,Nelt,th1,th2,th3,stresslim);

% Singularity evaluation function
fcn.singufcn = @(jac) SinguCorde3PFR(jac);

% Stability evaluation function
fcn.stabilityfcn = @(jac) StabilityCorde3PFR(jac);

% Robot internal energy function
fcn.internfcn = @(y) internalEnergy3PFR(y,k,th1,th2,th3);

%% First Initial Guess

[output,lA] = CCInverse3PFR(geoparams,pstart,th,Nelt,th1,th2,th3,L); % constant curvature approximation
y0 = [lA;output(3,2:end)';output(9,2:end)';output(15,2:end)';pstart;th;zeros(6,1)];   % constant curvature initial guess

options = optimoptions('fsolve','Display','iter-detailed');
fun = @(y) CordeIGSPeqn3PFR(y,k,Le,fend,mend,fd,pstart,th,geoparams,th1,th2,th3);
sol = fsolve(fun,y0,options);


params.y0 = sol;
[pos1,pos2,pos3] = pos3PRFCorde(sol,Nelt,Le,geoparams,th1,th2,th3);
PFR3Plot(pos1,pos2,pos3,pstart,th,geoparams,th1,th2,th3,L,lA1lim)

title('Initial Guess Configuration')
drawnow
