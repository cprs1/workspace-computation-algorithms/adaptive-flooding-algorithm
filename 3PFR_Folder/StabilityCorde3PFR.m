function flag = StabilityCorde3PFR(jac)
% jac is the full IGSP jacobian

H = jac(1:end-9,4:end-6); % extract correct components
Z = null(jac(end-8:end-3,4:end-6)); % null space of gradient of constraints
Hr = Z'*H*Z; % reduced hessian matrix
[~,flag] = chol(Hr); % attemp to do cholesky decomposition --> if flag = 0 is positive definite
end