%% INPUT FILE

%% Simulation parameters

guessflag = 'C';  %choose between K (bestkantorovich) or C (bestconditioning)
solverflag = 'Newton'; % choose betweeen Newton of 'fsolve' (trust-region)
maxiter = 10; % Maxiter of the Newton solver
TOL = 10^-6; % Tolerance for detecting singularities
TOL2 = Inf; % stop condition for max norm (set Inf to neglect it)
stepsize = 0.01; % Initial stepsize
stepmin = 0.01; % Minimum stepsize
boxsize = [-1 1 -1 1]; % size of the initial box



%% STORE

params.stepsize = stepsize;
params.stepmin = stepmin;
params.maxiter = maxiter;
params.boxsize = boxsize;
params.TOL = TOL;
params.TOL2 = TOL2;
params.guessflag = guessflag;
params.solver = solverflag;


