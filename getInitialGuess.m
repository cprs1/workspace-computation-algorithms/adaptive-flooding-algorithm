%% WORKSPACE COMPUTATION OF PLANAR CONTINUUM PARALLEL ROBOTS
% F. Zaccaria - Update 02 February 2022

% function to get the initial guess for the IGSP problem solution

% INPUTS:
% WK: workspace data array
% guesses: array that stacks previously computed robot configurations
% idmentry: indices of guess candidates
% pend: end-effector desired pose
% fcn: structure with objective functions
% params: structure with simulation parameters.

% OUTPUT:
% y0: initial guess robot configuration
% flagk: Kantorovich flag (2*chi*delta*lambda)
% idm: indices of initial guess in WK

function [y0,flagk,idm] = getInitialGuess(WK,guesses,idmentry,pend,params,fcn)
% extract infos from structures
guessflag = params.guessflag;
Cordefun = fcn.objetivefcn;
feq = @(y) Cordefun(y,pend);
Kantorovicfun = fcn.kantorovichfcn;

% switch in bewteen of different rules:

switch guessflag
        case 'C' % choose init. guess with best conditioning
        [~,idmin] = min(WK(idmentry,10));
        idm = idmentry(idmin);
        y0 = guesses(:,idm);
        [eq0,jac0] = feq(y0);
        [flagk,~,~,~] = Kantorovicfun(y0,eq0,jac0);
    case 'K' % choose init. guess with best Kantorovich flag
        tmp_flag = NaN*ones(numel(idmentry),1);
        kk = 0;
        check2 = 1;
        while (kk < numel(idmentry) && check2 == 1)
            kk = kk+1;
            idw2 = idmentry(kk);
            y0 = guesses(:,idw2);
            [eq0,jac0] = feq(y0);
            [tmp_flag(kk),~,~,~] = Kantorovicfun(y0,eq0,jac0);
            if tmp_flag(kk)<1
                check2 = 0;
            end
            
        end
        [~,idkk] = min(tmp_flag(kk));
        idm = idmentry(idkk);
        y0 = guesses(:,idm);
        flagk = tmp_flag(idkk);
    otherwise
        error('ERROR: define correctly guess rule (K or C)')
end
end