%% WORKSPACE COMPUTATION OF PLANAR CONTINUUM PARALLEL ROBOTS
% F. Zaccaria - Update 02 February 2022

% function to define a uniform grid in the plane:

% INPUTS:
% params: structure with simulation parameters.

% OUTPUTS:
% grid: array with grid points location

function grid = defineGrid(params) % planar grid definition
    boxsize = params.boxsize;
    stepsize = params.stepsize;
    
    xL = boxsize(1);
    xR = boxsize(2);
    yU = boxsize(3);
    yB = boxsize(4);
    n_sampX = floor(abs(xL-xR)/stepsize);
    n_sampY = floor(abs(yU-yB)/stepsize);
    x = linspace(xL,xR,n_sampX);
    yg = linspace(yB,yU,n_sampY);
    [X,Y] = meshgrid(x,yg);
    Xp = reshape(X,n_sampX*n_sampY,1);
    Yp = reshape(Y,n_sampX*n_sampY,1);
    grid = [Xp,Yp];
end