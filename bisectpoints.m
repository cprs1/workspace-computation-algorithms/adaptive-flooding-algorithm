%% WORKSPACE COMPUTATION OF PLANAR CONTINUUM PARALLEL ROBOTS
% F. Zaccaria - Update 02 February 2022

% function to bisect a box into 4 new boxes:

% INPUTS:

% pend: end-effector desired pose
% currentstep: stepsize of the box associated with pend

% OUTPUTS:

% points: 4 new points

function points = bisectpoints(pend,currentstep) % bisection function
     p1 = pend' + [+1,+1]*currentstep/4;
     p2 = pend' + [-1,+1]*currentstep/4;
     p3 = pend' + [-1,-1]*currentstep/4;
     p4 = pend' + [+1,-1]*currentstep/4;
     points = [p1;p2;p3;p4];
end