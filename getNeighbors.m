%% WORKSPACE COMPUTATION OF PLANAR CONTINUUM PARALLEL ROBOTS
% F. Zaccaria - Update 02 February 2022

% function to get neighbors of a grid point.

% INPUTS: 
% WK: workspace data array
% pstart: 2x1 vector --> point of which neighbors are found
% params: structure with simulation parameters.
% fact: scale factor for the neighbors radius (enlarge to get wider ranges)

% OUTPUT:
% idx: indices of the elements in WK that are neighbors

function idx = getNeighbors(WK,pstart,params,fact)
stepsize = params.stepsize;
id = ((abs(WK(:,2)-pstart(1))<=fact*stepsize) & (abs(WK(:,3)-pstart(2))<=fact*stepsize));
idx = WK(id==1,1);

end