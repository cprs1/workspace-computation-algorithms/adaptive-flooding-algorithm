%% WORKSPACE COMPUTATION OF PLANAR CONTINUUM PARALLEL ROBOTS
% F. Zaccaria - Update 02 February 2022

% function to manage the bisection process:

% INPUTS:

% actual_step: stepsize at the current iteration
% points_start: point where the iteration started
% pend: desired end-effector pose

% OUTPUTS:
% adds: 4x7 array containing the new lines to be added in WK
% I: permutation matrices for sorting of adds in WK

function [I,adds] = bisectionProcessManager(actual_step,pointstart,pend)

newpoints = bisectpoints(pend,actual_step);
adds = [newpoints(:,1:2), zeros(4,1),ones(4,1), zeros(4,6), ones(4,1)*actual_step/2;];

% sort by distance
dist_last =  ((newpoints(:,1)-pointstart(1)).^2+abs(newpoints(:,2)-pointstart(2)).^2).^(0.5);
[~,I] = sort(dist_last);

end